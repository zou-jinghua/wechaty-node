import ajax from './ajax.js'
import { FileBox } from 'file-box'

// 创建一个Map对象，用于存储条件和对应的函数
const functionMap = new Map();
let token = ''//填写你自己的微信对话平台token
let signature = '123'
const menu = FileBox.fromFile('./菜单.jpg')
//链接微信机器人
function wxjqrInit() {
    return new Promise(async (resolve, reject) => {
        let res = await ajax("https://chatbot.weixin.qq.com/openapi/sign/" + token, {
            userid: 1
        }, "post")
        signature = res.data.signature;
        resolve()
    });
}

//转语音函数
async function sayMP3(text, room, roleName) {
    roleName = roleName ? roleName : "雷电将军"
    let fileBox = null; //语音、文件
    let res = await ajax("http://api.lolimi.cn/API/yyhc/y.php?&msg=" + text + "&speaker=" + roleName,
        null, "get")
    fileBox = FileBox.fromUrl(res.data.music);
    await room.say(fileBox)
}
// url转图片
async function sayImg(room, url) {
    let fileBox = FileBox.fromUrl(url);
    await room.say(fileBox)
}

// 获取群聊天数
functionMap.set('getNumber', async function (room, text, msgNumber) {
    await room.say(`今天本群已聊消息数：${msgNumber}`)
});
//菜单
functionMap.set('getMenu', async function (room, ...agrs) {
    room.say(menu)
});
// 微信机器人
functionMap.set('weixinChatbot', async (room, text, agrs) => {
    let res = await ajax("https://chatbot.weixin.qq.com/openapi/aibot/" + token, {
        signature: signature,
        query: text,
    }, "post")
    if (res?.data?.errcode == 1005) {
        await wxjqrInit();
        res = await ajax("https://chatbot.weixin.qq.com/openapi/aibot/" + token, {
            signature: signature,
            query: text,
        }, "post")
    }
    let answer = res.data.answer
    if (res.data.status == "FAQ_RECOMMEND") {
        answer = "请问你是不是想问：" + res.data.options[0].title
    }
    await room.say(answer)
});
// 文字转语音
functionMap.set('text2mp3', async function (room, text, agrs) {
    sayMP3('转语音', room, text)

})

//参考这个形式去添加你自己的函数事件

// 根据条件调用对应的函数
const action = function callFunctionByCondition(eventname, room, text, msgNumber) {
    console.log('事件函数', eventname, text, msgNumber);
    if (functionMap.has(eventname)) {
        const func = functionMap.get(eventname);
        func(room, text, msgNumber);
    } else {
        console.log('未找到匹配的函数');
        room.say('小朋友，要乖哦，不可以乱点菜单，我会生气的。')
    }
}
export default action