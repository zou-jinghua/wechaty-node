
import { WechatyBuilder } from 'wechaty'
import QRcode from "qrcode"; //二维码
import { Cron } from "croner";
import action from './api.js';
import _ from 'lodash'
import qrcode from 'qrcode-terminal';
// 导入 Node.js 的文件系统模块
import { readFile } from 'fs/promises'; // 注意这里使用了 fs/promises 来支持 Promise 的文件操作



const name = 'wechat-puppet-wechat';
// let bot = '';
// bot = new Wechaty({
//   name, // generate xxxx.memory-card.json and save login data for the next login
// });
const wechaty = WechatyBuilder.build({ name }) // get a Wechaty instance

let msgobj = {}  //消息对象
let news = null  //每日新闻定时器
let newsobj = {}     //群聊room对象
let robot = null
let groupChat = ['测试']  //支持的群
/**
 * arr[Array]
 * 你自己的事件函数
 * */
let arr = ['getMenu', 'getNumber', "weixinChatbot"]


//每天重置信息数
function everyDate() {
    const job = new Cron('0 0 * * *', () => {
        console.log('消息数量已经重置');
        for (let key in msgobj) {
            msgobj[key] = 0;
        }
    });
}
// 每日新闻发送
function everynews() {
    //初始化的时候保存需要主动发送每日新闻的room群
    if (news === null) {
        try {
            //定时主动发送消息
            console.log('newsobj', newsobj);
            news = new Cron('0 0 8 * * *', () => {
                console.log('每日新闻');
                for (let key in newsobj) {
                    action(arr[8], newsobj[key], '', '')
                }

            });
        } catch (error) {
            console.log('每日新闻出错', error);
        }

    }
}
// 读取 JSON 文件的函数
async function readJSONFile() {
    try {
        // 使用 `readFile` 方法读取 JSON 文件
        const jsonData = await readFile('pyp.json', 'utf-8');

        // 将读取到的 JSON 字符串解析为 JavaScript 对象
        const parsedData = JSON.parse(jsonData);

        // 返回解析后的 JSON 数据
        return parsedData;
    } catch (error) {
        console.error('Error reading the file:', error);
        throw error; // 可以选择处理错误或向上层传递
    }
}

wechaty.on('scan', (qc, status) => {
    // qrcode.generate(qc); // 在console端显示二维码
    // const qrcodeImageUrl = [
    //   'https://wechaty.js.org/qrcode/',
    //   encodeURIComponent(qc),
    // ].join('');
    // console.log(qrcodeImageUrl);
    QRcode.toFile('./login.jpg', qc, err => {
        console.log("登陆图片生成完成")
    })
})
    .on('login', user => {
        console.log(`User ${user} logged in`)
        const regex = /<(.*?)>/;
        robot = regex.exec(user)
    })
    .on('message', async msg => {
        let text = msg.text()//消息内容
        // console.log('msg触发,文本内容：', text)
        let room = msg.room()//房间对象
        let msgType = msg.type() //7 是文本 6是图片 8是视频  https://wechaty.js.org/zh/docs/api/message
        if (msgType != 7 && msgType != 0) {//过滤非文本信息，日志太长不想看
            return
        }
        // console.log(msgType);
        if (room && text.length > 0) {//接收到了房间发送的消息
            let topic = await room.topic()  //群名称
            console.log('群聊触发')
            let user = false;
            let clap = text.indexOf('拍了拍我') !== -1 ? true : false;
            /**
             * 你的机器人在群里的名称
            */
            if (text.indexOf(`@你自己的机器人名称`) !== -1) {
                user = '@你自己的机器人名称 ';
            }
            //判断群聊
            if (groupChat.includes(topic) !== -1) {
                console.log('进入群聊，是否艾特机器人', user)
                //保存每个群的room对象
                if (!(newsobj.hasOwnProperty(topic))) {
                    console.log('room克隆触发', topic, newsobj)
                    newsobj[topic] = _.cloneDeep(room)
                }
                // 群聊消息数量+1，无数量则初始化
                if (!msgobj.hasOwnProperty(topic)) {
                    msgobj[topic] = 0
                } else {
                    msgobj[topic]++
                }
                //判断是否艾特机器人关键词
                if (user) {
                    // console.log('有人艾特机器人，机器人执行事件');
                    msgobj[topic]++  //机器人自己的发言也要计算
                    //判断事件类型，分割命令
                    let textarr = text.split(/(\d+)/);
                    let msgtext = textarr[2] || '';
                    for (let i = 3; i < textarr.length; i++) {
                        msgtext += textarr[i];
                    }
                    msgtext = msgtext.replace(/\s/g, "")
                    console.log('textarr', textarr);
                    //无命令则输出菜单
                    if (!textarr[1]) {
                        console.log('菜单');
                        await action(arr[0], room, msgtext, msgobj[topic])
                    } else {
                        //有命令根据arr的事件库传递命令事件和房间号，消息数量等信息
                        let nums = Number(textarr[1])
                        console.log('nums', nums, '-----', msgtext);
                        await action(arr[nums], room, msgtext, msgobj[topic])
                    }
                }
                if (clap) {
                    // console.log('拍一拍');
                    //你自己的微信名称
                    if (text.indexOf('不知道') !== -1) {
                        await room.say('主人轻点拍，我要被玩坏了~')
                        // console.log('主人轻点拍，我要被玩坏了~');
                    } else {
                        let pyptext = ''
                        // 调用读取 JSON 文件的函数
                        await readJSONFile()
                            .then(data => {
                                let index = Math.floor(Math.random() * data.responses.length);
                                pyptext = data.responses[index]
                            })
                        await room.say(pyptext)
                        // console.log(pyptext);
                    }
                }

            }
        }
        console.log('分割线-----------------------------');
    })

wechaty.start()


//初始化函数
function init() {
    everyDate()
    everynews()
}
init()
