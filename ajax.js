import { createAlova } from 'alova';
import { axiosRequestAdapter } from '@alova/adapter-axios';
import axios from 'axios';

let axiosOption = {
    timeout: 15000, // 15秒
    headers: {
        'Content-Type': 'application/json;charset=utf-8'
    }
};
let customAxios = axios.create(axiosOption)
let requestAdapter = axiosRequestAdapter()
const alovaInst = createAlova({
    requestAdapter,
    axios: customAxios,
    shareRequest: false,
    cacheLogger: () => {
        console.log('本次请求走缓存');
    },
    localCache: null
});

// 封装一个统一的接口请求
const ajax = async (url, param, type) => {
    return new Promise(async (resolve, reject) => {
        try {
            type = type ? type : 'get';
            if (type === 'get') {
                const res = await alovaInst.Get(url, param);
                resolve(res);
            } else if (type === 'post') {
                const res = await alovaInst.Post(url, param);
                resolve(res);
            }
        } catch (error) {
            console.log(error);
            reject(error);
        }
    });
};


export default ajax